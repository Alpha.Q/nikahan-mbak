from django.db import models

# Create your models here.

class Ucapan(models.Model):
    nama = models.CharField(max_length=255)
    alamat = models.CharField(max_length=255)
    is_hadir = models.BooleanField(default=False)
    jumlah_hadir = models.CharField(max_length=255)
    ucapan_doa = models.CharField(max_length=255)
