from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from nikahan.models import Ucapan
# Create your views here.

import requests

def telegram_bot_sendtext(bot_message):
    
    bot_token = '5086329444:AAEUeB0LlYzsUSwalswN37AcdbSps7J6T2Y'
    bot_chatID = '443178472'
    # bot_chatID = '918704839'
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message

    response = requests.get(send_text)

    return response.json()


def index(request, jadwal):
    print(jadwal)
    if(jadwal == "1"):
        waktu_resepsi = "10:00 s/d 11:00"
    elif(jadwal == "2"):
        waktu_resepsi = "11:00 s/d 12:00"
    else:
        waktu_resepsi = "12:00 s/d 13:00"

    print("waktu resepsi " + waktu_resepsi)
    print(request.GET)
    print(request.GET.get('untuk'))
    # print(nama)
    # print(r)
    ucapans = Ucapan.objects.all()

    idx = 0
    arr_ucapan = []
    for ucapan in ucapans:
        if ucapan.nama != "" and ucapan.ucapan_doa != "":
            arr_ucapan.append({'nama': ucapan.nama, 'doa': ucapan.ucapan_doa, 'index': idx})
            idx = idx + 1

    response = {'untuk': request.GET.get('untuk'),
                'waktu_resepsi': waktu_resepsi,
                'list_ucapan': arr_ucapan,
                'banyak': idx,
                }
    print(response)
    return render(request,'index.html',response)

def cleaned(x):
    y = x.replace("%0D%0A", "\n")
    y = y.replace("%2C", ",")
    return y    

def handleForm(request):
    print(request)
    print(request.POST)
    print(request.body)
    print(type(request.body))
    data_str = request.body.decode('utf-8')
    print(data_str)
    print(data_str)
    
    data_map = {}
    data_lst = data_str.split("&")
    for data in data_lst:
        now = data.split("=")
        data_map[now[0]] = " ".join(now[1].split('+'))
    print(data_map)
    print(type(data_map))

    nama = ""
    alamat = ""
    is_hadir = False
    jumlah_hadir = ""
    ucapan = ""

    if 'contactName' in data_map:
        nama = data_map['contactName']
    
    if 'contactAddress' in data_map:
        alamat = data_map['contactAddress']

    if 'totalKehadiran' in data_map:
        is_hadir = True
        jumlah_hadir = data_map['totalKehadiran']
    
    if 'doaMessage' in data_map:
        ucapan = data_map['doaMessage']

    nama = cleaned(nama)
    alamat = cleaned(alamat)
    jumlah_hadir = cleaned(jumlah_hadir)
    ucapan = cleaned(ucapan)

    Ucapan.objects.create(
        nama=nama,
        alamat=alamat,
        is_hadir=is_hadir,
        jumlah_hadir=jumlah_hadir,
        ucapan_doa=ucapan
    )

    test = telegram_bot_sendtext("Isian baru konfirmasi.\nNama: " + nama + "\n" + "Alamat: " + alamat + "\n" + "Apakah Hadir: " + str(is_hadir) + "\n" + "Jumlah Kehadiran: " + jumlah_hadir + "\n" + "Ucapan Doa: " + ucapan + "\n")
    print(test)

    print(nama)
    print(alamat)
    print(is_hadir)
    print(jumlah_hadir)
    print(ucapan)
     
    response = JsonResponse({'hasil': 'OK'})
    return response

